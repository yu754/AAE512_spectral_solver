#include <iostream>
#include <iomanip>
#include <vector>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string.h>
#include "MatMult.h"
#include "JacobiPoly.h"
#include "lapacke.h"
//#include "SEM1D.h"
#define PI 3.14159265


using namespace std;

/*SHOULD BE A PART OF MESH READER (PRE-PROCESSING)*/
/*quad ordering*/
/* 3---------2
   |    2    |
   |3       1|
   |         |
   |    0    |
   0---------1
*/
/*The only problem specific parts are x,y,z coordinates of the elements
The B matrix remains identical for all quad elements*/
const double alpha = 0.0;
const double beta = 0.0;  

class quad{
public:
    double *xi, *we; 
    int order, bdtypes[4], QuadratureOrder;
    quad(int P){
        order = P;
        //NeighElIDs = N;
        //X = x; 
        //Y = y; 
        //Z = z;
        QuadratureOrder = P+4;
        double *J = get_JACOBI(0.0, 0.0, QuadratureOrder);
        xi = get_EIGS(J, QuadratureOrder);
        we = get_WEIGHTS(J, QuadratureOrder, 0.0, 0.0);
    }
    
    int **ModalIndex(){
        int **MI = 0; int P = this->order;
        MI = new int*[2];
        MI[0] = new int[(P+1)*(P+1)]; MI[1] = new int[(P+1)*(P+1)];
        MI[0][0] = 0; MI[0][1] = P; MI[0][2] = P; MI[0][3] = 0;
        MI[1][0] = 0; MI[1][1] = 0; MI[1][2] = P; MI[1][3] = P;
        for (int i = 0; i<P-1; i++)
        {   MI[0][i+4] = i+1; MI[1][i+4] = 0;
            MI[0][i+4+P-1] = P; MI[1][i+4+P-1] = i+1;
            MI[0][i+4+2*(P-1)]=i+1; MI[1][i+4+2*(P-1)] = P;
            MI[0][i+4+3*(P-1)]=0; MI[1][i+4+3*(P-1)] = i+1;
        }
        int ctr = 4 + 4*(P-1);
        for (int i =0; i<P-1; i++)
        {
            for (int j=0; j<P-1; j++)
            {
                MI[0][ctr + i*(P-1)+j] = i+1;
                MI[1][ctr + i*(P-1)+j] = j+1;
            }
        }
        return MI;
    }
    int **NodalIndex(){
        int **NI = 0; int Q = this->QuadratureOrder;
        NI = new int*[2];
        NI[0] = new int[Q*Q]; NI[1] = new int[Q*Q];
        NI[0][0] = 0; NI[0][1] = Q-1; NI[0][2] = Q-1; NI[0][3] = 0;
        NI[1][0] = 0; NI[1][1] = 0; NI[1][2] = Q-1; NI[1][3] = Q-1;
        for (int i = 0; i<Q-2; i++)
        {
            NI[0][i+4] = i+1; NI[1][i+4] = 0;
            NI[0][i+4+Q-2] = Q-1; NI[1][i+4+Q-2] = i+1; 
            NI[0][i+4+2*(Q-2)]=i+1; NI[1][i+4+2*(Q-2)] = Q-1;
            NI[0][i+4+3*(Q-2)]=0; NI[1][i+4+3*(Q-2)] = i+1;
        } 
        int ctr = 4 + 4*(Q-2);
        for (int i = 0; i<Q-2; i++)
        {
            for (int j=0;j<Q-2; j++)
            {
                NI[0][ctr+i*(Q-2)+j] = i+1;
                NI[1][ctr+i*(Q-2)+j] = j+1;
            }
        }
        return NI;
    }

    double **get_1DModalBasis(){
        double **psi = 0; 
        psi = new double*[this->order+1];
        for (int i = 0; i<=this->order; i++)
        {
            psi[i] = get_C0MODALBASIS_a(this->xi, i, this->order, this->QuadratureOrder);
            //psi[i] = get_ORTMODALBASIS_a(this->xi, i, this->order, this->QuadratureOrder);
        }
    return psi;
    }

    double **get_BMatrix(){
        int **MI = this->ModalIndex(); int P = this->order; int Q = this->QuadratureOrder;
        int **NI = this->NodalIndex();
        double **B = 0; int N = (P+1)*(P+1); int M = Q*Q;
        double *xi_i = this->xi; 
        double **psi = this->get_1DModalBasis();
        B = new double*[M];
        for (int m = 0; m<M; m++)
        {   
            B[m] = new double[N];
            for (int n = 0; n<N; n++)
                B[m][n] = psi[MI[0][n]][NI[0][m]]*psi[MI[1][n]][NI[1][m]];
        }
        return B;
    }
    double **get_DiffMat(){
        return get_DIFFMATLEG(this->xi, this->QuadratureOrder);
    }
    double *get_phi0(){
        return get_C0MODALBASIS_a(this->xi, 0, this->order, this->QuadratureOrder);
    }
    double *get_phiP(){
        return get_C0MODALBASIS_a(this->xi, this->order, this->order, this->QuadratureOrder);
    }
    double **get_W(){
        double **W = 0; int Q = this->QuadratureOrder;
        int **NI = this->NodalIndex();
        double *we = this->we;
        W = new double*[Q*Q];
        for (int i = 0; i<Q*Q; i++)
        {   
            W[i] = new double[Q*Q];
            W[i][i] = we[NI[0][i]]*we[NI[1][i]];
        }
        return W;
    }
    ~quad(){
        cout<<"deleted element object";
    }
};


class element
{
    //0 bdytype for interior
    //1 bdytype for domain boundary    
   
    double X1[4], X2[4];
    int N;
    int P;
public:
    void SetValues(int order, double *x, double *y)
    {
        for (int i = 0; i<4; i++)
        {
            X1[i] = x[i]; X2[i] = y[i];
        }
        P = order;
    }
    void PrintValues()
    {
        cout<<"Order is: "<<this->P<<endl;
        for (int i =0; i<4; i++)
            cout<<this->X1[i]<<" , "<<this->X2[i]<<endl;
    }
    double **xy_xi(int index,  double *phi0, double *phiP, int Q)
    {   
        double **xatxi = 0;

        xatxi = new double*[Q];
        for (int i = 0; i<Q; i++)
        {
            xatxi[i] = new double[Q];
            for (int j = 0; j<Q; j++)
                if (index == 0)
                {    
                    xatxi[i][j] = this->X1[0]*phi0[i]*phi0[j] + this->X1[1]*phiP[i]*phi0[j] + this->X1[2]*phiP[i]*phiP[j] + this->X1[3]*phi0[i]*phiP[j];}
                else
                    xatxi[i][j] = this->X2[0]*phi0[i]*phi0[j] + this->X2[1]*phiP[i]*phi0[j] + this->X2[2]*phiP[i]*phiP[j] + this->X2[3]*phi0[i]*phiP[j];
        }
        return xatxi;
    }
    double **DeltaX(double **x, double **y, int Q)
    {
        double **dx; dx = new double*[4];
        for (int i = 0; i<4; i++)
            dx[i] = new double[2];

        dx[0][0] = x[Q-1][0] - x[0][0];
        dx[0][1] = y[Q-1][0] - y[0][0];

        dx[1][0] = x[Q-1][Q-1] - x[Q-1][0];
        dx[1][1] = y[Q-1][Q-1] - y[Q-1][0];

        dx[2][0] = x[Q-1][Q-1] - x[0][Q-1];
        dx[2][1] = y[Q-1][Q-1] - y[0][Q-1];

        dx[3][0] = x[0][Q-1] - x[0][0];
        dx[3][1] = y[0][Q-1] - y[0][0];

        return dx;

    }
    double **dxdxi1(double **x1atxi, double **D, int Q)
    {    return MatMult(D, x1atxi, Q, Q, Q);}

    double **dxdxi2(double **x1atxi, double **D, int Q)
    {   double **x1atxiT = Transpose(x1atxi, Q, Q);
        return Transpose(MatMult(D, x1atxiT, Q, Q, Q), Q, Q);}

    double **dydxi1(double **x2atxi, double **D, int Q)
    {   return MatMult(D, x2atxi, Q, Q, Q);}
    double **dydxi2(double **x2atxi, double **D, int Q)
    {   double **x2atxiT = Transpose(x2atxi, Q, Q); 
        return Transpose(MatMult(D, x2atxiT, Q, Q, Q), Q, Q); }

    double **get_Jac(double **d11, double **d12, double **d21, double **d22, int Q)
    {   
        double **Jac = 0;
        Jac = new double*[Q];
        for (int i =0; i<Q; i++)
        {   Jac[i] = new double[Q];
            for (int j = 0; j<Q; j++)
                Jac[i][j] = d11[i][j]*d22[i][j] - d12[i][j]*d21[i][j];
        }
        return Jac;
    }
    double *get_ForceVec(double **B, double **W, double **D, double **x1, double **x2, int **NI, int Q, int P)
    {
        double *WFe = 0; WFe = new double[Q*Q];
        double **d11 = this->dxdxi1(x1, D, Q);
        double **d12 = this->dxdxi2(x1, D, Q);
        double **d21 = this->dydxi1(x2, D, Q);
        double **d22 = this->dydxi2(x2, D, Q);
        double **Jac = this->get_Jac(d11, d12, d21, d22, Q);
        //cout<<"printing the force vector: "<<endl;
        for (int i =0; i<Q*Q; i++)
        {    
            //WFe[i] = W[i][i]*Jac[NI[0][i]][NI[1][i]]*(-1.0*(1.0+PI*PI)*sin(PI*x1[NI[0][i]][NI[1][i]]));
            //WFe[i] = W[i][i]*Jac[NI[0][i]][NI[1][i]]*(2.0 - pow(x1[NI[0][i]][NI[1][i]],2.0));
            //WFe[i] = W[i][i]*Jac[NI[0][i]][NI[1][i]]*(-1.0*x1[NI[0][i]][NI[1][i]]*x2[NI[0][i]][NI[1][i]]);
            //cout<<Jac[NI[0][i]][NI[1][i]]<<" , at: "<<x1[NI[0][i]][NI[1][i]]<<" , "<<x2[NI[0][i]][NI[1][i]]<<endl;
            //WFe[i] = -1.0*W[i][i]*Jac[NI[0][i]][NI[1][i]]*((1.0+2.0*PI*PI)*cos(PI*x1[NI[0][i]][NI[1][i]])*cos(PI*x2[NI[0][i]][NI[1][i]]));
            WFe[i] = -1.0*W[i][i]*Jac[NI[0][i]][NI[1][i]]*(750.0*sin(5.0*x1[NI[0][i]][NI[1][i]])*cos(7.0*x2[NI[0][i]][NI[1][i]]));
        }
        double **BT = Transpose(B, Q*Q, (P+1)*(P+1));
        double *Fe = MatColMult(BT, WFe, (P+1)*(P+1), Q*Q);
        //for (int i = 0 ; i<(P+1)*(P+1); i++)
        //    cout<<Fe[i]<<endl;
        return Fe;

    }
    double **get_MassMatrix(double **B, double **W, double **D, double **x1, double **x2, int **NI, int Q, int P)
    {   

        double **d11 = this->dxdxi1(x1, D, Q);
        double **d12 = this->dxdxi2(x1, D, Q);
        double **d21 = this->dydxi1(x2, D, Q);
        double **d22 = this->dydxi2(x2, D, Q);
        double **Jac = this->get_Jac(d11, d12, d21, d22, Q);
        double **WJac = 0; WJac = new double*[Q*Q];
        for (int i = 0; i<Q*Q; i++)
        {   
            WJac[i] = new double[Q*Q];
            for (int j =0; j<Q*Q; j++)
                WJac[i][j] = 0.0;
        }
        for (int i =0; i<Q*Q; i++)
            WJac[i][i]=W[i][i]*Jac[NI[0][i]][NI[1][i]]; 
        
        double **M1 = MatMult(WJac, B, Q*Q, Q*Q, (P+1)*(P+1));
        double **BT = Transpose(B, Q*Q, (P+1)*(P+1));
        double **Me = MatMult(BT, M1, (P+1)*(P+1), Q*Q, (P+1)*(P+1));
        return Me;
    }

    double **get_Lambda(double **dnn, double **Jac, int **NI, int Q)
    {
        double **lambda = 0; lambda = new double *[Q*Q];
        for (int i = 0; i<Q*Q; i++)
        {
            lambda[i] = new double [Q*Q];
            lambda[i][i] = dnn[NI[0][i]][NI[1][i]]/Jac[NI[0][i]][NI[1][i]];
        }
        return lambda;
    }
    double **get_Dxi1(double **D,int **NI, int Q)
    {
        double **Dxi = 0; Dxi = new double*[Q*Q];
        for (int m = 0; m<Q*Q; m++)
        {   
            Dxi[m] = new double[Q*Q];
            for (int n = 0; n<Q*Q; n++)
            {   
                Dxi[m][n] = 0.0;
                if (NI[1][m]==NI[1][n])
                    Dxi[m][n] = D[NI[0][m]][NI[0][n]];
            }
        }
        return Dxi;
    }
    double **get_Dxi2(double **D,int **NI, int Q)
    {
        double **Dxi = 0; Dxi = new double*[Q*Q];
        for (int m = 0; m<Q*Q; m++)
        {   
            Dxi[m] = new double[Q*Q];
            for (int n = 0; n<Q*Q; n++)
            {   
                Dxi[m][n] = 0.0;
                if (NI[0][m]==NI[0][n])
                    Dxi[m][n] = D[NI[1][m]][NI[1][n]];
            }
        }
        return Dxi;
    }

    double **get_Laplace(double **B, double **W, double **D, double **x1, double **x2, int **NI, int Q, int P)
    {
        double **d11 = this->dxdxi1(x1, D, Q);
        double **d12 = this->dxdxi2(x1, D, Q);
        double **d21 = this->dydxi1(x2, D, Q);
        double **d22 = this->dydxi2(x2, D, Q);
        double **Jac = this->get_Jac(d11, d12, d21, d22, Q);
        double **WJac = 0; WJac = new double*[Q*Q];
        for (int i = 0; i<Q*Q; i++)
        {   
            WJac[i] = new double[Q*Q];
            for (int j =0; j<Q*Q; j++)
                WJac[i][j] = 0.0;
        }
        for (int i =0; i<Q*Q; i++)
            WJac[i][i]=W[i][i]*Jac[NI[0][i]][NI[1][i]]; 
        
        double **lambda11 = this->get_Lambda(d22, Jac, NI, Q);
        double **lambda12 = this->get_Lambda(d12, Jac, NI, Q);
        double **lambda21 = this->get_Lambda(d21, Jac, NI, Q);
        double **lambda22 = this->get_Lambda(d11, Jac, NI, Q);
        double **Dxi1 = this->get_Dxi1(D, NI, Q);
        double **Dxi2 = this->get_Dxi2(D, NI, Q);
        double **Le11 = MatMult(MatMult(lambda11, Dxi1 , Q*Q, Q*Q, Q*Q), B, Q*Q, Q*Q, (P+1)*(P+1)); 
        double **Le21 = MatMult(MatMult(lambda21, Dxi2 , Q*Q, Q*Q, Q*Q), B, Q*Q, Q*Q, (P+1)*(P+1));

        double **Le12 = MatMult(MatMult(lambda12, Dxi1 , Q*Q, Q*Q, Q*Q), B, Q*Q, Q*Q, (P+1)*(P+1));
        double **Le22 = MatMult(MatMult(lambda22, Dxi2 , Q*Q, Q*Q, Q*Q), B, Q*Q, Q*Q, (P+1)*(P+1));

        double **Le1 = Matadd(Le11, Le21, Q*Q, (P+1)*(P+1), 1);
        double **Le2 = Matadd(Le22, Le12, Q*Q, (P+1)*(P+1), 1);

        double **Le = Matadd(ATBA(Le1, WJac, Q*Q, (P+1)*(P+1)) , ATBA(Le2, WJac, Q*Q, (P+1)*(P+1)), (P+1)*(P+1), (P+1)*(P+1), 0);

        return Le;
    }
    /*All edges are mapped in order
    edge = 0 => xi2=-1 => j=0
    edge = 1 => xi1=1 => i=Q-1
    edge = 2 => xi2=1 => j=Q-1
    edge = 3 => xi1=-1 => i=0*/
    double **get_EdgeCoords(double **x1, double **x2, int Q, int P, int edge)
    {
        double **xyS = 0;
        xyS = new double*[2];
        xyS[0] = new double[Q]; xyS[1] = new double[Q];
        if (edge==0)
        {   
            for (int i =0; i<Q; i++)
            {    xyS[0][i] = x1[i][0]; xyS[1][i] = x2[i][0];}
        }
        else if (edge==1)
        {
            for (int i =0; i<Q; i++)
            {    xyS[0][i] = x1[Q-1][i]; xyS[1][i] = x2[Q-1][i];}
        }
        else if (edge ==2)
        {
            for (int i =0; i<Q; i++)
            {    xyS[0][i] = x1[i][Q-1]; xyS[1][i] = x2[i][Q-1];}
        }   
        else if (edge == 3)
        {
            for (int i =0; i<Q; i++)
            {    xyS[0][i] = x1[0][i]; xyS[1][i] = x2[0][i];}
        }
        
        return xyS;
    }
    double *get_EdgeJac(double **x1, double **x2, double **D, int Q, int P, int edge)
    {
        double *SJac = 0; SJac = new double[Q];
        double **d11 = this->dxdxi1(x1, D, Q);
        double **d12 = this->dxdxi2(x1, D, Q);
        double **d21 = this->dydxi1(x2, D, Q);
        double **d22 = this->dydxi2(x2, D, Q);

        if (edge == 0)
        {
            for (int i = 0; i<Q; i++)
            {    SJac[i] = sqrt(pow(d11[i][0], 2) + pow(d21[i][0],2));}
        }
        else if (edge == 1)
        {
            for (int i = 0; i<Q; i++)
            {    SJac[i] = sqrt(pow(d12[Q-1][i], 2) + pow(d22[Q-1][i],2));}
        }
        else if (edge == 2)
        {
            for (int  i =0; i<Q; i++)
            {    SJac[i] = sqrt(pow(d11[i][Q-1], 2) + pow(d21[i][Q-1],2));}
        }
        else if (edge == 3)
        {
            for (int i = 0; i<Q; i++)
            {    SJac[i] = sqrt(pow(d12[0][i], 2) + pow(d22[0][i],2));}
        }
        return SJac;
        
    }
    double *get_EdgeBcon(double **xyS, int Q, int P, int edge, int bdytag)
    {   
        double *gS = 0;
        gS = new double[Q];
        for (int i = 0; i<Q; i++)
        {
            if (bdytag==0)
            {    
                //gS[i] = -PI*cos(PI*xyS[0][i])/sqrt(5.0);
                //gS[i] = -(xyS[1][i]+2.0*xyS[0][i])/sqrt(5.0);
                //gS[i] = PI*(sin(PI*xyS[0][i])*cos(PI*xyS[1][i]) + 2.0*cos(PI*xyS[0][i])*sin(PI*xyS[1][i]))/sqrt(5.0);
                //gS[i] = PI*cos(PI*xyS[0][i])*sin(PI*xyS[1][i]);
                //gS[i] = -xyS[0][i];
                gS[i] = (-50.0*cos(5.0*xyS[0][i])*cos(7.0*xyS[1][i]) + 140.0*sin(5.0*xyS[0][i])*sin(7.0*xyS[1][i]))/sqrt(5.0);
            }
            else if(bdytag == 1)
            {    //gS[i] = 4.0*PI*cos(PI*xyS[0][i])/sqrt(17.0);
                //gS[i] = (4.0*xyS[1][i]+xyS[0][i])/sqrt(17.0);
                //gS[i] = 1.0;
                gS[i] = (200.0*cos(5.0*xyS[0][i])*cos(7.0*xyS[1][i]) - 70.0*sin(5.0*xyS[0][i])*sin(7.0*xyS[1][i]))/sqrt(17.0);
                //gS[i] = -PI*sin(PI*xyS[0][i])*cos(PI*xyS[1][i]);
                //gS[i] = xyS[1][i];
            }
            else if(bdytag == 2)
            {    //gS[i] = 0.0;
                //gS[i] = 0.0;
                //gS[i] = (xyS[1][i] + 3.0*xyS[0][i])/sqrt(10.0);
                //gS[i] = -PI*(sin(PI*xyS[0][i])*cos(PI*xyS[1][i])+3.0*cos(PI*xyS[0][i])*sin(PI*xyS[1][i]))/sqrt(10.0);
                gS[i] = -70.0*sin(5.0*xyS[0][i])*sin(7.0*xyS[1][i]);
                //gS[i] = xyS[0][i];
            }
            else if(bdytag == 3)
            {    
                //gS[i] = -2.0*PI*cos(PI*xyS[0][i])/sqrt(5.0);
                //gS[i] = (-2.0*xyS[1][i] + xyS[0][i])/sqrt(5.0);
                gS[i] = (-100.0*cos(5.0*xyS[0][i])*cos(7.0*xyS[1][i]) - 70.0*sin(5.0*xyS[0][i])*sin(7.0*xyS[1][i]))/sqrt(5.0);
                //gS[i] = PI*sin(PI*xyS[0][i])*cos(PI*xyS[1][i]);
                //gS[i] = -xyS[1][i];
            }
        }
        return gS;
    }

    ~element()
    {
        cout<<"deleting element"<<endl;
    }
    
};

int main()
{   
    quad *elq; 

    int P = 14;             
    ofstream f, f1;

    f1.open("../Output/Solutions.dat");

    f.open("../Output/QuadraturePoints.dat");

    elq = new quad(P);
    int Q = elq->QuadratureOrder;
    //double **Me = elq->get_MassMatrix();
    double **B = elq->get_BMatrix();
    double **B_1D = elq->get_1DModalBasis();
    double **D = elq->get_DiffMat();
    double *phi0 = elq->get_phi0();
    double *phiP = elq->get_phiP();
    double **W = elq->get_W();
    int **MI = elq->ModalIndex();
    int **NI = elq->NodalIndex();
    double *we = elq->we;
    double *xi = elq->xi;

    cout<<"Zongxin printing xx points"<<endl;
    for (int i = 0 ; i<Q; i++)
        cout<<(1.0 + xi[i])/2.0<<endl;
    cout<<"Go fuck yourself";

/******mesh parameters***********reading from text file to be automated later******/
    /*

    const int Noofvert = 9; 
    const int Noofel = 4; 
    const int Noofed = 12; 
    const int NoofBdys = 4;

    double **verts = 0; verts = new double*[2];
    verts[0] = new double[Noofvert]; verts[1] = new double[Noofvert];
    double Xverts[] = {-1.0, 1.0, 0.5, -0.5, 0.0, 0.75, 0.0, -0.75, 0.0};
    double Yverts[] = {-1.0, -2.0, 0.0, 0.0, -1.5, -1.0, 0.0, -0.5, -0.75};
    
    for (int i =0 ;i<Noofvert; i++)
    {    verts[0][i] = Xverts[i]; 
        verts[1][i] = Yverts[i];
    }

    int VertsOfElements[Noofel][4] = {{0, 4, 8, 7}, 
                                      {8, 4, 1, 5},
                                      {2, 6, 8, 5},
                                      {7, 8, 6, 3}};

    int EdgesofElements[Noofel][4] = {{0,8,11,7},
                                      {8,1,2,9},
                                      {4,10,9,3},
                                      {11,10,5,6}};

    double DeltaXeElements[Noofel][4][2];                          //containts directionality of all 4 edges of all elements

    int Edge2Boundary[Noofed];
    for (int i = 0; i<Noofed; i++)
    {
        Edge2Boundary[i]= -1;
    }
    

    Edge2Boundary[0] = 0; Edge2Boundary[1] = 0;
    Edge2Boundary[2] = 1; Edge2Boundary[3] = 1;
    Edge2Boundary[4] = 2; Edge2Boundary[5] = 2;
    Edge2Boundary[6] = 3; Edge2Boundary[7] = 3;

/***********************************************************/

/****1. single element test mesh parameters*****************/
    // const int Noofvert = 4; 
    // const int Noofel = 1; 
    // const int Noofed = 4; 
    // const int NoofBdys = 4;

    // double **verts = 0; verts = new double*[2];
    // verts[0] = new double[Noofvert]; verts[1] = new double[Noofvert];
    // // double Xverts[] = {-1.0, 0.0, 0.0, -0.75};
    // // double Yverts[] = {-1.0, -1.5, -0.75, -0.5};
    // double Xverts[] = {-1.0, 1.0, 1.0, -1.0};
    // double Yverts[] = {-1.0, -1.0, 1.0, 1.0};
    
    // for (int i =0 ;i<Noofvert; i++)
    // {    verts[0][i] = Xverts[i]; 
    //     verts[1][i] = Yverts[i];
    // }
    
    // int VertsOfElements[Noofel][4] = {{3, 0, 1, 2}};

    // int EdgesofElements[Noofel][4] = {{3,0,1,2}};
    // // int VertsOfElements[Noofel][4] = {{0, 1, 2, 3}};

    // // int EdgesofElements[Noofel][4] = {{0, 1, 2, 3}};
    // double DeltaXeElements[Noofel][4][2];                          //containts directionality of all 4 edges of all elements

    // int Edge2Boundary[Noofed];
    // for (int i = 0; i<Noofed; i++)
    // {
    //     Edge2Boundary[i]= -1;
    // }
  
    // Edge2Boundary[0] = 0;
    // Edge2Boundary[1] = 1;
    // Edge2Boundary[2] = 2;
    // Edge2Boundary[3] = 3;
/**************************************************************/
/******2. two element test mesh parameters*********************/
    // const int Noofvert = 6; 
    // const int Noofel = 2; 
    // const int Noofed = 7; 
    // const int NoofBdys = 4;

    // double **verts = 0; verts = new double*[2];
    // verts[0] = new double[Noofvert]; verts[1] = new double[Noofvert];
    // double Xverts[] = {-1.0, 0.0, 1.0, 0.75,  0.0, -0.75};
    // double Yverts[] = {-1.0, -1.5, -2.0, -1.0, -0.75, -0.5};

    // // double Xverts[] = {-1.0, 0.0, 1.0, 1.0,  0.0, -1.0};
    // // double Yverts[] = {-1.0, -1.0, -1.0, 1.0, 1.0, 1.0};
    
    
    // for (int i =0 ;i<Noofvert; i++)
    // {    
    //     verts[0][i] = Xverts[i]; 
    //     verts[1][i] = Yverts[i];
    // }
    
    // int VertsOfElements[Noofel][4] = {{0, 1, 4, 5},
    //                                   {4, 1, 2, 3}};

    // int EdgesofElements[Noofel][4] = {{0, 6, 4, 5},
    //                                   {6, 1, 2, 3}};
    // // int VertsOfElements[Noofel][4] = {{0, 1, 4, 5},
    // //                                   {1, 2, 3, 4}};

    // // int EdgesofElements[Noofel][4] = {{0, 6, 4, 5},
    // //                                   {1, 2, 3, 6}};

    // double DeltaXeElements[Noofel][4][2];                          //containts directionality of all 4 edges of all elements

    // int Edge2Boundary[Noofed];
    // for (int i = 0; i<Noofed; i++)
    // {
    //     Edge2Boundary[i]= -1;
    // }
  
    // Edge2Boundary[0] = 0;
    // Edge2Boundary[1] = 0;
    // Edge2Boundary[2] = 1;
    // Edge2Boundary[3] = 2;
    // Edge2Boundary[4] = 2; 
    // Edge2Boundary[5] = 3;
/**************************************************************/
    /*
    signed int ElementsofEdges[Noofed][4];
    int TagEdges[Noofed][4];
    int flag[Noofed];
    for (int i =0; i<Noofed; i++)
    {    
        flag[i] = 0;
        ElementsofEdges[i][0] = -1; ElementsofEdges[i][1] = -1; ElementsofEdges[i][2] = -1; ElementsofEdges[i][3] = -1;
    }    

    for (int e=0;e<Noofel;e++)
    {
        for (int edge = 0; edge<4; edge++)
        {   
            if (flag[EdgesofElements[e][edge]] == 0)
            {   
                ElementsofEdges[EdgesofElements[e][edge]][0] = e;
                ElementsofEdges[EdgesofElements[e][edge]][1] = edge;
                flag[EdgesofElements[e][edge]] = 1;
            }
            else
            {    
                ElementsofEdges[EdgesofElements[e][edge]][2] = e;
                ElementsofEdges[EdgesofElements[e][edge]][3] = edge;
            }
        }
    }
    /*TagEdges tags the edges
    index runs according to the global numbering of the edges
    at 0     1->interior edge with positive sign
            -1->interior edge with negative sign for odd modes
             0->edge touching boundary
    at 1     left element
             -1 for edge touching the boundary
    at 2     right element
             -1 for edge touching the boundary
    at 3    global boundary name (tagged with numbers)*/
    /*
    int ge, e_L, e_R, edge_L, edge_R, k, v0_e, v1_e, v0_k, v1_k;
    for (int i =0; i<Noofed; i++)
    {    
        e_L = ElementsofEdges[i][0]; edge_L = ElementsofEdges[i][1]; 
        e_R = ElementsofEdges[i][2]; edge_R = ElementsofEdges[i][3];

        TagEdges[i][0] = 0;       // zero for boundary edges and 1 for interior edges
        TagEdges[i][1] = e_L; 
        TagEdges[i][2] = e_R;
        TagEdges[i][3] = -1;      // -1 for interior edges and Boundary number for boundary edges
        if ((e_L!=-1)&&(e_R!=-1)) //Interior edge
            TagEdges[i][0] = 1;
        else                      //Boundary edge TagEdges[i][3] contains the boundary name (number)
            TagEdges[i][3] = Edge2Boundary[i];
    }
    

    int totalModes = Noofvert + (P-1)*Noofed + (P-1)*(P-1)*Noofel;
    cout<<"total degrees of freedom: "<<totalModes<<endl;
    int **VertexToMode = 0; 
    int **EdgeToMode = 0; 
    int **ElementToMode = 0;
 
    EdgeToMode = new int*[Noofed];
    for (int i =0; i<Noofed; i++)
    {   
        EdgeToMode[i] = new int[P-1];
        for (int j =0; j<P-1; j++)
            EdgeToMode[i][j] = Noofvert + (P-1)*i+j;
    }
    ElementToMode = new int*[Noofel];
    for(int e = 0; e<Noofel; e++)
    {
        ElementToMode[e] = new int[(P-1)*(P-1)];
        for (int i = 0; i<(P-1)*(P-1); i++)
            ElementToMode[e][i] = Noofvert + (P-1)*Noofed + e*(P-1)*(P-1) + i;

    }
    double X[4], Y[4];

    element qu[Noofel];
    for (int e = 0; e<Noofel; e++)
    {   

        for (int i =0; i<4; i++)
        {
            X[i] = verts[0][VertsOfElements[e][i]];
            Y[i] = verts[1][VertsOfElements[e][i]];
        }
        qu[e].SetValues(P, X, Y);
        double **x_xi = qu[e].xy_xi(0, phi0, phiP, Q);
        double **y_xi = qu[e].xy_xi(1, phi0, phiP, Q);
        double **DeltaXEdges = qu[e].DeltaX(x_xi, y_xi, Q);

        for (int i =0; i<4; i++)
        {
            for (int j =0;j<2;j++)
            {    
                DeltaXeElements[e][i][j] = DeltaXEdges[i][j];
            }
        }
 
    }

    int **Map = 0; int **sign = 0;
    Map = new int*[Noofel];
    sign = new int*[Noofel];

    for (int e =0; e<Noofel; e++)
    {   
        Map[e] = new int[(P+1)*(P+1)];
        sign[e] = new int[(P+1)*(P+1)];
        for (int v = 0; v<4; v++)
        {
            Map[e][v] = VertsOfElements[e][v];
            sign[e][v] = 1;
        }

        for (int edge = 0; edge<4; edge++)
        {
            for (int j=0; j<P-1; j++)
            {
                int i = edge*(P-1)+j+4;
                Map[e][i] = EdgeToMode[EdgesofElements[e][edge]][j];
                if(TagEdges[EdgesofElements[e][edge]][0] == 1)          //if interior edge
                {
                    e_L = ElementsofEdges[EdgesofElements[e][edge]][0];
                    e_R = ElementsofEdges[EdgesofElements[e][edge]][2];

                    int e_L_localedge = ElementsofEdges[EdgesofElements[e][edge]][1];
                    int e_R_localedge = ElementsofEdges[EdgesofElements[e][edge]][3];

                    double innerProd = DeltaXeElements[e_L][e_L_localedge][0]*DeltaXeElements[e_R][e_R_localedge][0] + DeltaXeElements[e_L][e_L_localedge][1]*DeltaXeElements[e_R][e_R_localedge][1];
                    if (innerProd<0.0)
                    {   
                        if (e==e_L)
                            sign[e][i] = pow(-1,j);
                        else
                            sign[e][i] = 1;
                    }
                    else
                        sign[e][i] = 1;
                }
                else
                    sign[e][i] = 1;
            }
        }
        for (int i =0; i<(P-1)*(P-1); i++)
        {
            Map[e][4*P + i] = ElementToMode[e][i];
            sign[e][4*P + i] = 1;
        }
    }
    double **M_global = 0; double **L_global = 0;
    double *gamma_global = 0; double *F_global = 0;

    M_global = new double*[totalModes];
    L_global = new double*[totalModes];
    gamma_global = new double[totalModes];
    F_global = new double[totalModes];

    for (int i =0 ; i<totalModes; i++)
    {   
        gamma_global[i] = 0.0;
        F_global[i] = 0.0;
        M_global[i] = new double[totalModes];
        L_global[i] = new double[totalModes];
        for (int j =0 ; j<totalModes; j++)
        {   
            L_global[i][j] = 0.0; 
            M_global[i][j] = 0.0;
        }
    }
    */
    /*
   

    int bdytag;
    double u_analytical[Noofel][Q*Q];
    for (int e = 0; e<Noofel; e++)
    {   
        cout<<endl<<"processing element: "<<e<<endl;
        double **x_xi = qu[e].xy_xi(0, phi0, phiP, Q);
        double **y_xi = qu[e].xy_xi(1, phi0, phiP, Q);

        // for (int m = 0; m<Q; m++)
        // {   
        //     for (int n = 0; n<Q; n++)
        //         f<<x_xi[m][n]<<" ,"<<y_xi[m][n]<<endl;
        // }
       for (int i =0; i<Q*Q; i++)
        {
            f<<x_xi[NI[0][i]][NI[1][i]]<<","<<y_xi[NI[0][i]][NI[1][i]]<<endl;
        }
        for (int i =0; i<Q*Q; i++)
        {
            // f<<x_xi[NI[0][i]][NI[1][i]]<<","<<y_xi[NI[0][i]][NI[1][i]]<<endl;
            //u_analytical[e][i] = sin(PI*(x_xi[NI[0][i]][NI[1][i]]));
            //u_analytical[e][i] = cos(PI*x_xi[NI[0][i]][NI[1][i]])*cos(PI*y_xi[NI[0][i]][NI[1][i]]);
            u_analytical[e][i] = 10.0*sin(5.0*x_xi[NI[0][i]][NI[1][i]])*cos(7.0*y_xi[NI[0][i]][NI[1][i]]);
        }
    //     f<<"*********************************************************************"<<endl;
        double *Fe = qu[e].get_ForceVec(B, W, D, x_xi, y_xi, NI, Q, P);
        double **Me = qu[e].get_MassMatrix(B, W, D, x_xi, y_xi, NI, Q, P);
        double **Le = qu[e].get_Laplace(B, W, D, x_xi, y_xi, NI, Q, P);
        for (int i= 0; i<(P+1)*(P+1); i++)
        {   
            F_global[Map[e][i]]+=sign[e][i]*Fe[i];
            for (int j =0;j<(P+1)*(P+1); j++)
            {    
                L_global[Map[e][i]][Map[e][j]]+= sign[e][i]*sign[e][j]*Le[i][j];
                M_global[Map[e][i]][Map[e][j]]+= sign[e][i]*sign[e][j]*Me[i][j];
            }
        }
        */
        /*
        for (int edge = 0; edge<4; edge++)
        {      
            if (TagEdges[EdgesofElements[e][edge]][0]==0) //Boundary edge detected...contributes to neumann conditions.
            {   
                bdytag = TagEdges[EdgesofElements[e][edge]][3];
                double **xyS = qu[e].get_EdgeCoords(x_xi, y_xi, Q, P, edge);

                double *g = qu[e].get_EdgeBcon(xyS, Q, P, edge, bdytag);
                
                double *Sj = qu[e].get_EdgeJac(x_xi, y_xi, D, Q, P, edge);
                double *SWG = 0; SWG = new double[Q];
                double l = 0.0;
                for (int i =0; i<Q; i++)
                {    
                    SWG[i] = Sj[i]*we[i]*g[i];
                    l+= Sj[i]*we[i];
                }

                double *gamma_S = MatColMult(B_1D, SWG, P+1, Q);

                if ((edge==0)||(edge==1))
                {
                    gamma_global[Map[e][edge]]+=sign[e][edge]*gamma_S[0]; 
                    gamma_global[Map[e][edge+1]]+=sign[e][edge+1]*gamma_S[P];
                } 
                else
                {
                    gamma_global[Map[e][edge]]+=sign[e][edge]*gamma_S[P];
                    gamma_global[Map[e][(edge+1)%4]]+=sign[e][(edge+1)%4]*gamma_S[0];
                }

                for (int j = 0; j<P-1; j++)
                    gamma_global[Map[e][4+edge*(P-1)+j]]+=sign[e][4+edge*(P-1)+j]*gamma_S[j+1];

                delete [] gamma_S;
            }
        }            
        delete [] Le; 
        delete [] Me;
        delete [] Fe;
        delete [] x_xi;
        delete [] y_xi;
    }
    f.close();

    double *RHS = 0; RHS = new double[totalModes];
    for (int i =0; i<totalModes; i++)
        RHS[i] = gamma_global[i] - F_global[i];
 
    const double lam = 1.0;
    double **H = Matadd(M_global, L_global, totalModes, totalModes, 0);

    double A_lhs[totalModes*totalModes];
    for (int i =0; i<totalModes*totalModes; i++)
        A_lhs[i] = 0.0;

    for (int i =0; i<totalModes; i++)
    {
        for (int j=0;j<totalModes; j++)
            A_lhs[j*totalModes+i] = H[i][j];
    }
    int ipiv[totalModes], info;
    int n = totalModes; int n2 = 1;
    cout<<"solving the linear system...."<<endl;
 
    dgesv_(&n, &n2, A_lhs, &n, ipiv, RHS, &n, &info);


    // f.close();

    

    for (int e = 0; e<Noofel; e++)
    {   
        double uhats_e[(P+1)*(P+1)];
        for (int j = 0; j<(P+1)*(P+1); j++)
            uhats_e[j] = sign[e][j]*RHS[Map[e][j]];

        double *ue_quad = MatColMult(B, uhats_e, Q*Q, (P+1)*(P+1));
        //cout<<"printing solution for element: "<<e<<endl;
        for (int m =0; m<Q*Q; m++)
            f1<<ue_quad[m]<<" , "<<u_analytical[e][m]<<" , "<<ue_quad[m]-u_analytical[e][m]<<endl;

        // if (e==0)
        // {
        //     for (int m = 0; m<Q-2; m++)
        //         f1<<ue_quad[4+2*(Q-2)+m]<<endl;
        // }
        // else if (e==1)
        // {
        //     for (int m = 0; m<Q-2; m++)
        //         f1<<ue_quad[4+3*(Q-2)+m]<<endl;
        // }
    }
    f1.close();
    */
    
    return 0;
}

