#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <cmath>


//Implement divide and conquer for addition and multiplication
double** Matadd(double **A, double **B, int x, int y, int option)
{
    double **S = 0; S = new double*[x];
    for (int i =0;i<x; i++)
    {   
        S[i] = new double[y];
        for (int j = 0; j<y; j++)
        {   if (option == 0) 
                S[i][j] = A[i][j] + B[i][j];
            else
                S[i][j] = A[i][j] - B[i][j];
        }
    }
    return S;
}
double* MatColMult(double** M, double* v , int m, int n)
{
    double* Prod = 0; Prod = new double [m];
    for (int i =0; i<m; i++)
    {
        Prod[i] = 0.0;
        for(int j=0; j<n; j++)
            Prod[i]+=M[i][j]*v[j];
    }

    return Prod;
}
double* RowMatMult(double** M, double* v , int m, int n)
{
    double* Prod = 0; Prod = new double [n];
    for (int i =0; i<n; i++)
    {
        Prod[i] = 0.0;
        for(int j=0; j<m; j++)
            Prod[i]+=M[j][i]*v[j];
    }

    return Prod;
}

/*this is upto user to specify proper argument list*/
double** MatMult(double** A, double** B, int x, int y, int z)
{   

    double** Prod = 0; Prod = new double*[x];
    for (int i=0; i<x; i++)
    {   
        Prod[i] = new double[z];
        for (int j = 0; j<z; j++)
        {   
            Prod[i][j] = 0.0;
            for (int k=0; k<y; k++)
                Prod[i][j] += A[i][k]*B[k][j];
        }
    }
    return Prod;
}
double** Transpose1Mult(double** A, double** B, int x, int y, int z)
{   
    //[A]yx [B]yz
    double** Prod = 0; Prod = new double*[x];
    for (int i=0; i<x; i++)
    {   
        Prod[i] = new double[z];
        for (int j = 0; j<z; j++)
        {   
            Prod[i][j] = 0.0;
            for (int k=0; k<y; k++)
                Prod[i][j] += A[k][i]*B[k][j];
        }
    }
    return Prod;
}
double** ATBA(double** A, double** B, int x, int y) 
{   
    //[A]xy [B]xx [A]xy = [A.T B A]yy 
    double **Prod1 = MatMult(B, A, x, x, y);
    // double** Prod1 = 0; Prod1 = new double*[x];
    // for (int i=0; i<x; i++)
    // {   
    //     Prod1[i] = new double[y];
    //     for (int j = 0; j<y; j++)
    //     {   
    //         Prod1[i][j] = 0.0;
    //         for (int k=0; k<x; k++)
    //             Prod1[i][j] += B[i][k]*A[k][j];
    //     }
    // }
    double **Prod = Transpose1Mult(A, Prod1, y, x, y);
    return Prod;
}
double** TensProd(double *a, double *b, int x, int y)
{
    double** Prod = 0; Prod = new double*[x];
    for (int i = 0; i<x; i++)
    {
        Prod[i] = new double[y];
        for (int j = 0; j<y; j++)
            Prod[i][j] = a[i]*b[j];
    }
    return Prod;
}
double** Transpose(double **A, int x, int y)
{   
    /***do not change A****/
    double **AT = 0;
    AT = new double*[y];
    for (int i = 0; i<y;i++)
    {   AT[i] = new double[x];
        for (int j = 0; j<x;j++)
            AT[i][j] = A[j][i];           
    }
    return AT;
}