import sys
import os
import numpy as np 
import math as ma
import scipy
import copy
from pdb import set_trace
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy.interpolate import griddata
import matplotlib.tri as tri
import umesh_reader
import matplotlib.cm as cm
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection







ONE_D=0
TWO_D=0
TEST_MAP=1
TEST_MAP2=0
TEST_2D =0

fig_name = 'one.png'
fig_width = 6
fig_height = 6
textFontSize   = 10
gcafontSize    = 20
lineWidth      = 2  
if TWO_D:

	c_list=np.zeros((5,2))
	c_list[0,:]=np.array([10,6 ])
	c_list[1,:]=np.array([6 ,10])	
	c_list[2,:]=np.array([2 ,6])	
	c_list[3,:]=np.array([6 ,2])	
	c_list[4,:]=np.array([6 ,6])		

	fig_name = 'one.png'
	database={}

	database['x']=np.loadtxt('mesh_x.dat')
	database['y']=np.loadtxt('mesh_y.dat')
	database['phi']=np.loadtxt('solution_19000.dat')

	x = database['x']
	y = database['y']
	phi = database['phi']

#	plt.tricontourf(triang, phi.flatten().T)
#	plt.show()

	XX, YY = np.meshgrid(x, y)	
	n=200
	xg = np.linspace(x.min(),x.max(),n)
	yg = np.linspace(y.min(),y.max(),n)
	X,Y = np.meshgrid(xg,yg)


	Z = griddata(np.vstack((x.flatten(),y.flatten())).T, phi.flatten().T, \
			        (X,Y), method='cubic').reshape(X.shape)
	# mask nan values, so they will not appear on plot


	center1 = c_list[0,:]
	radius1 = 1
	DIST = ((X - center1[0])**2.0 + (Y - center1[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	center2 = c_list[1,:]				
	radius1 = 1
	DIST = ((X - center2[0])**2.0 + (Y - center2[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan						

	center3 = c_list[2,:]				
	radius1 = 1
	DIST = ((X - center3[0])**2.0 + (Y - center3[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	center4 = c_list[3,:]				
	radius1 = 1
	DIST = ((X - center4[0])**2.0 + (Y - center4[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	center5 = c_list[4,:]				
	radius1 = 1
	DIST = ((X - center5[0])**2.0 + (Y - center5[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	Zm = np.ma.masked_where(np.isnan(Z),Z)	
	triang = tri.Triangulation(x.flatten(), y.flatten())
	triang = tri.Triangulation(x.flatten(),y.flatten())



	# Create the Triangulation; no triangles so Delaunay triangulation created.


	# Mask off unwanted triangles.
	x=x.flatten()
	y=y.flatten()
	set_trace()

	levels = np.arange(0., 5, 0.025)
	cmap = cm.get_cmap(name='RdBu_r', lut=None)

	fig = plt.figure(0,figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111, aspect='equal')		

	#fig.colorbar(p, ax=ax)
	c=ax.pcolormesh(X,Y,Zm,shading='gouraud')	







	patches = []

	circle = Circle((c_list[0,0], c_list[0,1]), 1)
	patches.append(circle)

	circle = Circle((c_list[1,0], c_list[1,1]), 1)
	patches.append(circle)

	circle = Circle((c_list[2,0], c_list[2,1]), 1)
	patches.append(circle)

	circle = Circle((c_list[3,0], c_list[3,1]), 1)
	patches.append(circle)

	circle = Circle((c_list[4,0], c_list[4,1]), 1)
	patches.append(circle)        
	
	colors = np.ones(len(patches))*100
	cmap1 = cm.get_cmap(name='Greys', lut=None)
	p = PatchCollection(patches, alpha=1,cmap=cmap1,edgecolor="none")

	p.set_array(np.array(colors))
	#ax.add_collection(p)






	cbar= fig.colorbar(c)
	cbar.ax.tick_params(labelsize=gcafontSize)							

	c.set_clim(0, 5)

	cbar.ax.set_ylabel(r'$\phi$', fontsize=1.2*gcafontSize)
	cl = plt.getp(cbar.ax, 'ymajorticklabels')
	plt.setp(cl, fontsize=gcafontSize) 				

	ax.set_xlabel(r'$x$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$y$',fontsize=1.2*gcafontSize)
	#plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	#plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)	


#	ax.set_xticks([])
#	ax.set_yticks([])
	fig.tight_layout()


	plt.savefig(fig_name)
	plt.show()
	plt.close()
	print fig_name+' saved!'	


if TEST_2D:
	fig_name = 'ini.png'
	database={}

	database['x']=np.loadtxt('mesh_x.dat')
	database['y']=np.loadtxt('mesh_y.dat')
	database['phi']=np.loadtxt('initial.dat')
	#phi_analytical = 
	x = database['x']
	y = database['y']
	phi = database['phi']	
	ana_x=-2*np.pi*np.cos(2*np.pi*x)*np.sin(2*np.pi*y)

	n=20
	xg = np.linspace(x.min(),x.max(),n)
	yg = np.linspace(y.min(),y.max(),n)
	X,Y = np.meshgrid(xg,yg)
	# interpolate Z values on defined grid
	Z = griddata(np.vstack((x.flatten(),y.flatten())).T, phi.flatten().T, \
					(X,Y), method='cubic').reshape(X.shape)
	ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, ana_x.flatten().T, \
					(X,Y), method='cubic').reshape(X.shape)	
	# mask nan values, so they will not appear on plot
	xxx = np.linspace(0,1,n)
	yyy = np.linspace(0,1,n)	
	XX,YY = np.meshgrid(x,y)	
	#ZZ=-np.cos(np.pi*XX)*np.sin(np.pi*YY)


	Zm = np.ma.masked_where(np.isnan(Z),Z)	
	ZZm = np.ma.masked_where(np.isnan(ZZ),ZZ)

	fig = plt.figure(0,figsize=(fig_width,fig_height))
	ax = fig.add_subplot(121, aspect='equal')
	ax2 = fig.add_subplot(122, aspect='equal')
	c=ax.pcolormesh(X,Y,Zm,shading='gouraud')	
	c2 = ax2.pcolormesh(X,Y,ZZm,shading='gouraud')	

	cbar= fig.colorbar(c)
	#c2bar = fig.colorbar(c2)

	cbar.ax.tick_params(labelsize=gcafontSize)							

	#c.set_clim(0, 3)

	cbar.ax.set_ylabel(r'$/phi$', fontsize=1.2*gcafontSize)
	cl = plt.getp(cbar.ax, 'ymajorticklabels')
	plt.setp(cl, fontsize=gcafontSize) 				

	ax.set_xlabel(r'$x$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$y$',fontsize=1.2*gcafontSize)
	#plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	#plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)	


#	ax.set_xticks([])
#	ax.set_yticks([])
	fig.tight_layout()


	plt.savefig(fig_name)
	plt.show()
	plt.close()
	print fig_name+' saved!'	
	set_trace()

if ONE_D:

	database={}

	database['F_sol_1']=np.loadtxt('solution_500.dat')
	database['F_sol_2']=np.loadtxt('solution_1500.dat')
	database['F_sol_3']=np.loadtxt('solution_3000.dat')
	F_sol_1=database['F_sol_1']
	F_sol_2=database['F_sol_2']
	F_sol_3=database['F_sol_3']
	figname ='solution.png'
	fig = plt.figure(0,figsize=(fig_width,fig_height))#figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111)	

	plt.plot(F_sol_1[:,0],F_sol_1[:,1],'--r',linewidth=lineWidth,label='FTCS')
	plt.plot(F_sol_2[:,0],F_sol_2[:,1],'--g',linewidth=lineWidth,label='FTCS')
	plt.plot(F_sol_3[:,0],F_sol_3[:,1],'--k',linewidth=lineWidth,label='FTCS')



	#handles, labels = ax.get_legend_handles_labels()
	#ax.legend(handles, labels)
	plt.ylabel('u',fontsize=gcafontSize*0.8)
	plt.xlabel('x',fontsize=gcafontSize*0.8)
	#	ax.set_title('Explicit FTCS')
	#plt.legend([aa,cc,bb], ('absolut error','ref_line : 2th order','ref_line : 1th order'),loc='best')
	plt.grid(True, which='both')		#	
	plt.tight_layout()
	plt.savefig(figname)




	plt.show()
	plt.close()
	print figname+' saved!'

	set_trace()



if TEST_MAP	:
	filename = "./mesh_folder/final.msh"
	#filename = 'nine.msh'
	#filename = 'one.msh'
	#filename = 'two.msh'
	#filename  = 'one_skew.msh'
	#filename = 'quad.msh'
	mshfile_fullpath = filename#icemcfd_project_folder + filename



	part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = \
			umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
		 
	  #####################################################
	  ########## Plot Grid Labels / Connectivity ##########
	  ##################################################### 

	Plot_Node_Labels = False
	Plot_Face_Labels = False
	Plot_CV_Labels   = False
	mgplx = 0.05*np.abs(max(xy_no[:,0])-min(xy_no[:,0]))
	mgply = 0.05*np.abs(max(xy_no[:,1])-min(xy_no[:,1]))
	xlimits = [min(xy_no[:,0])-mgplx,max(xy_no[:,0])+mgplx]
	ylimits = [min(xy_no[:,1])-mgply,max(xy_no[:,1])+mgply] 

	fig = plt.figure(0,figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111)
	ax.plot(xy_no[:,0],xy_no[:,1],'o',markersize=5,markerfacecolor='k') 

	
	for inos_of_fa in noofa:
	   ax.plot(xy_no[inos_of_fa,0], xy_no[inos_of_fa,1], 'k-', linewidth = lineWidth) 		
	ax.axis('equal')

	fig_name = 'one_map.png'
	database={}

	database['x']=np.loadtxt('mesh_x.dat')
	database['y']=np.loadtxt('mesh_y.dat')

	x = database['x']
	y = database['y']
	phi = np.ones(x.shape)*5				
	n=10
	xg = np.linspace(0,2,n)
	yg = np.linspace(0,2,n)
	X,Y = np.meshgrid(xg,yg)



	ax.plot(x,y,'k.')
		
	ax.axis('equal')
	ax.set_xlabel(r'$x$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$y$',fontsize=1.2*gcafontSize)
	#plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	#plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)	


	#ax.set_xticks([])
	#ax.set_yticks([])
	fig.tight_layout()


	plt.savefig(fig_name)
	plt.show()
	plt.close()
	print fig_name+' saved!'		



if TEST_MAP2	:

	filename = "./mesh_folder/single11.msh"
	#filename = 'nine.msh'
	#filename = 'one.msh'
	#filename = 'two.msh'
	#filename  = 'one_skew.msh'
	#filename = 'quad.msh'
	mshfile_fullpath = filename#icemcfd_project_folder + filename



	part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = \
			umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
		 
	  #####################################################
	  ########## Plot Grid Labels / Connectivity ##########
	  ##################################################### 

	Plot_Node_Labels = False
	Plot_Face_Labels = False
	Plot_CV_Labels   = False
	mgplx = 0.05*np.abs(max(xy_no[:,0])-min(xy_no[:,0]))
	mgply = 0.05*np.abs(max(xy_no[:,1])-min(xy_no[:,1]))
	xlimits = [min(xy_no[:,0])-mgplx,max(xy_no[:,0])+mgplx]
	ylimits = [min(xy_no[:,1])-mgply,max(xy_no[:,1])+mgply] 

	fig = plt.figure(0,figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111)
	ax.plot(xy_no[:,0],xy_no[:,1],'o',markersize=5,markerfacecolor='k') 
	node_color = 'k'
	centroid_color = 'r'  



	fig_name = 'nine_map.png'
	database={}

	database['x']=np.loadtxt('mesh_x.dat')
	database['y']=np.loadtxt('mesh_y.dat')

	x = database['x']
	y = database['y']
	phi = np.ones(x.shape)*5				
	n=10
	xg = np.linspace(0,2,n)
	yg = np.linspace(0,2,n)
	X,Y = np.meshgrid(xg,yg)


	fig = plt.figure(0,figsize=(fig_width,fig_height))#figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111)	

	ax.plot(x[0:11],y[0:11],'r+')
	ax.plot(x[11:22],y[11:22],'g+')
	ax.plot(x[22:33],y[22:33],'b+')
	ax.plot(x[33:44],y[33:44],'y+')			
	for inos_of_fa in noofa:
	   ax.plot(xy_no[inos_of_fa,0], xy_no[inos_of_fa,1], 'k-', linewidth = lineWidth) 		
	ax.axis('equal')
	ax.set_xlabel(r'$x$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$y$',fontsize=1.2*gcafontSize)
	#plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	#plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)	


	#ax.set_xticks([])
	#ax.set_yticks([])
	fig.tight_layout()


	plt.savefig(fig_name)
	plt.show()
	plt.close()
	print fig_name+' saved!'		