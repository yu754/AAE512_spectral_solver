import sys
import os
import numpy as np 
import math as ma
import scipy
import copy
from pdb import set_trace
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy.interpolate import griddata
import matplotlib.tri as tri
import umesh_reader
import matplotlib.cm as cm
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection

fig_name = 'test.png'
fig_width = 6
fig_height = 6
textFontSize   = 10
gcafontSize    = 20
lineWidth      = 1  


c_list=np.zeros((5,2))
c_list[0,:]=np.array([10,6 ])
c_list[1,:]=np.array([6 ,10])	
c_list[2,:]=np.array([2 ,6])	
c_list[3,:]=np.array([6 ,2])	
c_list[4,:]=np.array([6 ,6])		

database={}

database['x']=np.loadtxt('./../data/case_a/mesh_x.dat')
database['y']=np.loadtxt('./../data/case_a/mesh_y.dat')
xa = database['x']
ya = database['y']
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')
phi1 =np.loadtxt('./../data/case_a/solution_20000.dat')