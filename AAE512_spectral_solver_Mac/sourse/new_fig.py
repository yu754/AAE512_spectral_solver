import sys
import os
import numpy as np 
import math as ma
import scipy
import copy
from pdb import set_trace
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy.interpolate import griddata
import matplotlib.tri as tri
import umesh_reader
import matplotlib.cm as cm
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection

TWO_D 		=1
LINE 		=0
TEST_MAP	=0
MESH 		=0


fig_name = 'test.png'
fig_width = 6
fig_height = 6
textFontSize   = 10
gcafontSize    = 20
lineWidth      = 2  



c_list=np.zeros((5,2))
c_list[0,:]=np.array([10,6 ])
c_list[1,:]=np.array([6 ,10])	
c_list[2,:]=np.array([2 ,6])	
c_list[3,:]=np.array([6 ,2])	
c_list[4,:]=np.array([6 ,6])		

database={}

database['x']=np.loadtxt('./../data/mesh_x.dat')
database['y']=np.loadtxt('./../data/mesh_y.dat')
database['phi']=np.loadtxt('./../data/solution_30000.dat')
fig_name='./../figure/evo30000.png'
#database['phi']=np.loadtxt('./../data/initial.dat')
x = database['x']
y = database['y']
phi = database['phi']

#	plt.tricontourf(triang, phi.flatten().T)
#	plt.show()

XX, YY = np.meshgrid(x, y)	
n=200
xg = np.linspace(x.min(),x.max(),n)
yg = np.linspace(y.min(),y.max(),n)
X,Y = np.meshgrid(xg,yg)

if TWO_D:
	Z = griddata(np.vstack((x.flatten(),y.flatten())).T, phi.flatten().T, \
			        (X,Y), method='cubic').reshape(X.shape)
	# mask nan values, so they will not appear on plot
	center1 = c_list[0,:]
	radius1 = 1
	DIST = ((X - center1[0])**2.0 + (Y - center1[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	center2 = c_list[1,:]				
	radius1 = 1
	DIST = ((X - center2[0])**2.0 + (Y - center2[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan						

	center3 = c_list[2,:]				
	radius1 = 1
	DIST = ((X - center3[0])**2.0 + (Y - center3[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	center4 = c_list[3,:]				
	radius1 = 1
	DIST = ((X - center4[0])**2.0 + (Y - center4[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	center5 = c_list[4,:]				
	radius1 = 1
	DIST = ((X - center5[0])**2.0 + (Y - center5[1])**2.0)**0.5
	Z[np.where(DIST < radius1)] = np.nan	

	Zm = np.ma.masked_where(np.isnan(Z),Z)	

	fig = plt.figure(0,figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111, aspect='equal')		


	c=ax.pcolormesh(X,Y,Zm,shading='gouraud')	
	cbar= fig.colorbar(c)
	cbar.ax.tick_params(labelsize=gcafontSize)							
	c.set_clim(0, 5)

	cbar.ax.set_ylabel(r'$\phi$', fontsize=1.2*gcafontSize)
	cl = plt.getp(cbar.ax, 'ymajorticklabels')
	plt.setp(cl, fontsize=gcafontSize) 				

	ax.set_xlabel(r'$x$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$y$',fontsize=1.2*gcafontSize)
	#plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	#plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)	

#	ax.set_xticks([])
#	ax.set_yticks([])
	fig.tight_layout()


	plt.savefig(fig_name)
	plt.show()
	plt.close()
	print fig_name+' saved!'	






	######################################################

	######################################################	

	ax = fig.add_subplot(122)

	newx=np.linspace(0,12,200)
	newy=12-newx
	t=newx*2**0.5
		#set_trace()
	Z = griddata(np.vstack((x.flatten(),y.flatten())).T, phi.flatten().T, \
 		     (newx,newy), method='linear').reshape(newx.shape)	

	radius1 = 1	
	center2 = c_list[4,:]				
	DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
	Z[np.where(DIST < radius1)] = np.nan		

	Zm = np.ma.masked_where(np.isnan(Z),Z)	
	ax.plot(t,Zm,'k',linewidth=lineWidth)

	ax.axvline(2**0.5*6-1, color='k',alpha=0.6,linestyle='--')
	ax.axvline(2**0.5*6+1, color='k',alpha=0.6,linestyle='--')

	plt.xlabel(r'$y$',fontsize=1.5*gcafontSize)
	plt.ylabel(r'$\phi$',fontsize=1.5*gcafontSize)
	ax.set_xlim([0,np.max(t)])
	ax.set_ylim([0,7])		


	fig_name ='line.png'
	figure_path = '../report/figures/'
	fig_fullpath = figure_path + fig_name
	fig.tight_layout()	
	plt.savefig(fig_name)
	plt.show()
	plt.close()
	print fig_name+'saved!'
















if TEST_MAP	:
	filename = "./../mesh_folder/final.msh"
	mshfile_fullpath = filename#icemcfd_project_folder + filename

	part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = \
			umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
		 
	  #####################################################
	  ########## Plot Grid Labels / Connectivity ##########
	  ##################################################### 

	mgplx = 0.05*np.abs(max(xy_no[:,0])-min(xy_no[:,0]))
	mgply = 0.05*np.abs(max(xy_no[:,1])-min(xy_no[:,1]))
	xlimits = [min(xy_no[:,0])-mgplx,max(xy_no[:,0])+mgplx]
	ylimits = [min(xy_no[:,1])-mgply,max(xy_no[:,1])+mgply] 

	fig = plt.figure(0,figsize=(fig_width,fig_height))
	ax = fig.add_subplot(111)
	ax.plot(xy_no[:,0],xy_no[:,1],'o',markersize=5,markerfacecolor='k') 

	
	for inos_of_fa in noofa:
	   ax.plot(xy_no[inos_of_fa,0], xy_no[inos_of_fa,1], 'k-', linewidth = lineWidth) 		
	ax.axis('equal')
	database={}

	database['x']=np.loadtxt('./../data/mesh_x.dat')
	database['y']=np.loadtxt('./../data/mesh_y.dat')

	x = database['x']
	y = database['y']
	phi = np.ones(x.shape)*5				
	n=10
	xg = np.linspace(0,2,n)
	yg = np.linspace(0,2,n)
	X,Y = np.meshgrid(xg,yg)

	ax.plot(x,y,'k.')
		
	ax.axis('equal')
	ax.set_xlabel(r'$x$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$y$',fontsize=1.2*gcafontSize)
	#plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	#plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)	

	#ax.set_xticks([])
	#ax.set_yticks([])

	fig_name ='real_mesh.png'
	figure_path = './../figure/'
	fig_fullpath = figure_path + fig_name
	fig.tight_layout()	
	plt.savefig(fig_fullpath)
	plt.close()
	print fig_name+' saved!'	

if MESH:
	Nj=6
	N =Nj-1
	base=np.arange(Nj)*1.0
	base2=np.arange(Nj-1)*1.0
	xl=0.5*(1-np.cos(np.pi*base/N))
	xg=0.5*(1-np.cos(np.pi*(2*base2+1)/(2*N)))
	ug=np.zeros((N,N))
	ul=np.zeros((N+1,N+1))

	GX,GY=np.meshgrid(xg,xg)
	LX,LY=np.meshgrid(xl,xl)	
	fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.8))
	ax = fig.add_subplot(111)	

	for i in range(GX.shape[0]):
		ax.axvline(GY[i,0],color='r',linestyle='-')
		ax.axhline(GX[0,i],color='r',linestyle='-')
	for i in range(LX.shape[0]):
		ax.axvline(LY[i,0],color='k',linestyle='--')
		ax.axhline(LX[0,i],color='k',linestyle='--')
	plt.plot(GX.flatten(),GY.flatten(),'or')	
	plt.plot(LX.flatten(),LY.flatten(),'sk')		
	ax.set_xlim([0.0, 1.0])
	ax.set_ylim([0.0, 1.0])
	plt.xlabel(r'$X$',fontsize=1.5*gcafontSize)
	plt.ylabel(r'$Y$',fontsize=1.5*gcafontSize)
	fig_name ='mesh.png'
	figure_path = './../figure/'
	fig_fullpath = figure_path + fig_name
	fig.tight_layout()	
	plt.savefig(fig_fullpath)
	plt.close()
	print fig_name+' saved!'
	
