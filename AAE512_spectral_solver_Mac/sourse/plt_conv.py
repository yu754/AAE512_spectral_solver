import sys
import os
import numpy as np 
import math as ma
import scipy
import copy
from pdb import set_trace
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy.interpolate import griddata
import matplotlib.tri as tri
import umesh_reader
import matplotlib.cm as cm
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection


fig_name = 'test.png'
fig_width = 6
fig_height = 6
textFontSize   = 10
gcafontSize    = 20
lineWidth      = 1  


c_list=np.zeros((5,2))
c_list[0,:]=np.array([10,6 ])
c_list[1,:]=np.array([6 ,10])	
c_list[2,:]=np.array([2 ,6])	
c_list[3,:]=np.array([6 ,2])	
c_list[4,:]=np.array([6 ,6])		

database={}

database['x']=np.loadtxt('./../data/case_a/mesh_x.dat')
database['y']=np.loadtxt('./../data/case_a/mesh_y.dat')
database['phi']=np.loadtxt('./../data/case_a/solution_20000.dat')
xa = database['x']
ya = database['y']
phia = database['phi']

database['x']=np.loadtxt('./../data/case_b/mesh_x.dat')
database['y']=np.loadtxt('./../data/case_b/mesh_y.dat')
database['phi']=np.loadtxt('./../data/case_b/solution_110000.dat')
xb = database['x']
yb = database['y']
phib = database['phi']

database['x']=np.loadtxt('./../data/case_c//mesh_x.dat')
database['y']=np.loadtxt('./../data/case_c/mesh_y.dat')
database['phi']=np.loadtxt('./../data/case_c/solution_95000.dat')
xc = database['x']
yc = database['y']
phic = database['phi']

database['x']=np.loadtxt('./../data/case_d//mesh_x.dat')
database['y']=np.loadtxt('./../data/case_d/mesh_y.dat')
database['phi']=np.loadtxt('./../data/case_d/solution_70000.dat')
xd = database['x']
yd = database['y']
phid = database['phi']

fig = plt.figure(0,figsize=(fig_width*2,fig_height*0.8))
ax = fig.add_subplot(121)	



newy=np.linspace(0,12,200)
newx=np.ones(newy.shape)*6	
#. A
##############################################
Z = griddata(np.vstack((xa.flatten(),ya.flatten())).T, phia.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[1,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[3,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(newy,Zm,'g:',linewidth=lineWidth*2)

#. B
##############################################	
Z = griddata(np.vstack((xb.flatten(),yb.flatten())).T, phib.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[1,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[3,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(newy,Zm,'b.',mfc="None",linewidth=lineWidth)
#. C
##############################################	
Z = griddata(np.vstack((xc.flatten(),yc.flatten())).T, phic.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[1,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[3,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(newy,Zm,'r+',linewidth=lineWidth)
##############################################	
Z = griddata(np.vstack((xd.flatten(),yd.flatten())).T, phid.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[1,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[3,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan	

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(newy,Zm,'k-',linewidth=lineWidth)

ax.axvline(1, color='k',alpha=0.6,linestyle='--')
ax.axvline(3, color='k',alpha=0.6,linestyle='--')
ax.axvline(5, color='k',alpha=0.6,linestyle='--')
ax.axvline(7, color='k',alpha=0.6,linestyle='--')
ax.axvline(9, color='k',alpha=0.6,linestyle='--')
ax.axvline(11,color='k',alpha=0.6,linestyle='--')

plt.xlabel(r'$y$',fontsize=1.5*gcafontSize)
plt.ylabel(r'$\phi$',fontsize=1.5*gcafontSize)
ax.set_xlim([0,12])
ax.set_ylim([0,7])	


######################################################

######################################################	

ax = fig.add_subplot(122)

newx=np.linspace(0,12,200)
newy=12-newx
t=newx*2**0.5
#. A
##############################################
Z = griddata(np.vstack((xa.flatten(),ya.flatten())).T, phia.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan		

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(t,Zm,'g:',linewidth=lineWidth*2,label='N_block= 56,Ng=5')

#. B
##############################################
Z = griddata(np.vstack((xb.flatten(),yb.flatten())).T, phib.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan		

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(t,Zm,'b.',mfc="None",linewidth=lineWidth,label='N_block= 56,Ng=10')
##############################################
Z = griddata(np.vstack((xc.flatten(),yc.flatten())).T, phic.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan		

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(t,Zm,'r+',linewidth=lineWidth,label='N_block=112,Ng=5')
##############################################
Z = griddata(np.vstack((xd.flatten(),yd.flatten())).T, phid.flatten().T, \
		     (newx,newy), method='linear').reshape(newx.shape)	
radius1 = 1	
center2 = c_list[4,:]				
DIST = ((newx - center2[0])**2.0 + (newy - center2[1])**2.0)**0.5	
Z[np.where(DIST < radius1)] = np.nan		

Zm = np.ma.masked_where(np.isnan(Z),Z)	
ax.plot(t,Zm,'k-',linewidth=lineWidth,label='N_block=112,Ng=10')

ax.axvline(2**0.5*6-1, color='k',alpha=0.6,linestyle='--')
ax.axvline(2**0.5*6+1, color='k',alpha=0.6,linestyle='--')
handles,lables=ax.get_legend_handles_labels()
ax.legend(handles,lables,fontsize=gcafontSize*0.5,loc='best',numpoints=1)
#ax.legend(bbox_to_anchor=(0.,1.02,1.,.102),loc=3,ncol=2,mode="expand")

plt.xlabel(r'$l$',fontsize=1.5*gcafontSize)
plt.ylabel(r'$\phi$',fontsize=1.5*gcafontSize)
ax.set_xlim([0,np.max(t)])
ax.set_ylim([0,7])		


fig_name ='line.png'
figure_path = '../report/figures/'
fig_fullpath = figure_path + fig_name
fig.tight_layout()	
plt.savefig(fig_name)
plt.show()
plt.close()
print fig_name+'saved!'

